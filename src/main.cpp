#include <dpp/dpp.h>
#include <algorithm>
#include <cstdlib>
#include <iostream>
#include <iterator>
#include <sstream>
#include <string>
#include <toml.hpp>
#include <vector>

size_t uiLevenshteinDistance(const std::string& str1, const std::string& str2) {
  const size_t m(str1.size()), n(str2.size());

  if (m == 0)
	return n;
  if (n == 0)
	return m;

  // allocation below is not ISO-compliant,
  // it won't work with -pedantic-errors.
  size_t costs[n + 1];

  for (size_t k = 0; k <= n; k++)
	costs[k] = k;

  size_t i{0};
  for (char const& c1 : str1) {
	costs[0] = i + 1;
	size_t corner{i}, j{0};
	for (char const& c2 : str2) {
	  size_t upper{costs[j + 1]};
	  if (c1 == c2)
		costs[j + 1] = corner;
	  else {
		size_t t(upper < corner ? upper : corner);
		costs[j + 1] = (costs[j] < t ? costs[j] : t) + 1;
	  }

	  corner = upper;
	  j++;
	}
	i++;
  }

  return costs[n];
}

int main() {
  /* Get the name and location of our configuration file. */
  const std::string fname = std::string(getenv("HOME")) + "/cerberus.toml";

  /* Parse our configuration file. */
  const toml::value data = toml::parse(fname);
  /* Read the values inside of our configuration file so that we can use them
   * and change them on the fly. */
  const auto token = toml::find<toml::string>(data, "token");
  /* List of phrases to blacklist, from our configuration file :o */
  const auto blacklist =
	  toml::find<std::vector<std::string>>(data, "blacklist");
  /* Creating the bot instance */
  dpp::cluster bot(token,

				   dpp::i_default_intents | dpp::i_message_content);
  /* Logs stuff, self explanatory */
  bot.on_log(dpp::utility::cout_logger());

  /* Message Handler */
  bot.on_message_create([&](const dpp::message_create_t& event) {
	/* String-Stream the content of a new message */
	std::stringstream ss(event.msg.content);
	/* Let's make this string into a vector, I guess */
	std::istream_iterator<std::string> begin(ss);
	std::istream_iterator<std::string> end;
	std::vector<std::string> msgContent(begin, end);

	/* Read our list of blacklisted phrases, assign them a name. */
	for (std::string bItem : blacklist) {
	  /* Read the message's content, which is a string, split word-by-word
	   * into a vector.*/
	  for (std::string mContent : msgContent) {
		std::string str1{mContent}, str2{bItem};
		/* Calculate final similarity to filtered item */
		int editDistance = uiLevenshteinDistance(str1, str2);
		/* If a message contains a filtered word, or tries to bypass via
		 * adding/removing/replacing a letter, delete it.*/
		if (editDistance <= 1) {
		  bot.message_delete(event.msg.id, event.msg.channel_id);
		}
	  }
	}
  });

  bot.start(false);
}
