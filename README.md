Planned to be a moderation-focused discord bot, using D++ (https://dpp.dev) as an API, coded in c++, built using xmake (https://xmake.io/#/) and clang. (https://clang.llvm.org/) Using the C++17 Standard, And intended to be specialized in reliable filtering, performance and simplicity.


Uses TOML for configuration.
(https://github.com/ToruNiina/toml11)

Just clone that to your home directory, needed to build this of course.
Also, follow the installation instructions for D++ (https://dpp.dev/buildlinux.html)


Basic usage: compile this, slap the executable somewhere memorable (or optionally just run `xmake run`), make it executable, and make a config file (cerberus.toml) in the user's home directory, with your token inside of it. like so: `token = "blah blah"`; also, put down the phrases you need filtered like so: `blacklist = ["word," "word", "word"]`
